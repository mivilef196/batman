# coding:utf-8
from batman.functions import Ishigami
from batman.input_output import formater

io = formater('npy')

params = io.read('./batman-coupling/sample-space.npy', ['x1', 'x2', 'x3'])
X1, X2, X3 = params[0, :]

f_ishigami = Ishigami()
data = f_ishigami([X1, X2, X3])

io = formater('npy')
io.write('./batman-coupling/sample-data.npy', data, ['F'])
